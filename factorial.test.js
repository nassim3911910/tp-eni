const factorial = require("./factorial.js");

test("factorial 1 equals 1", () => {
  expect(factorial(1)).toBe(1);
});

test("factorial 5 equals 120", () => {
  expect(factorial(5)).toBe(120);
});
